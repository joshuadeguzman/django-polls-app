from django.db import models
import datetime
from django.utils import timezone

# Create your models here.


class Question(models.Model):
    # max_length: User defined
    question = models.CharField(max_length=300)

    # Parameter passed is an optional positional argument
    # Can be used as a human-readable name representation on a field
    # https://stackoverflow.com/questions/27077125/what-is-the-meaning-of-string-argument-in-django-models-field
    pub_date = models.DateTimeField('Date Published')

    # __str__ provides meaningful string representation used in Django's automatically-generated form
    def __str__(self):
        return self.question

    # Django model classes also allows to include custom methods used in querying objects
    # This method returns a boolean if object was created recently (within the day as seen in the parameter)
    def was_published_recently(self):
        now = timezone.now()
        return now - datetime.timedelta(days=1) < self.pub_date <= now

    was_published_recently.admin_order_field = 'pub_date'
    was_published_recently.boolean = True
    was_published_recently.short_description = "Published Recently?"


class Choice(models.Model):
    # Provides reference to the Question object
    # Django automatically creates related object set with respect to the question object
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)

    def __str__(self):
        return self.choice_text
